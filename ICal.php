<?php

/**
 * Requires sendmail for sendign emails
 * important functions :
 * ImportICaltoDB : imports input file to db
 * downloadICS : makes a call to converttoICS and downloads ics file
 * converttoICS : makes a call to db and generates ics 
 */

use Shoptimize_Bookingsystem_Model_Calendar;


class Shoptimize_Bookingsystem_Model_ICal extends Mage_Core_Model_Abstract
 {
    public /** @type(int) */ $event_count;
    public /** @type (array) */ $cal;
    public /** @type (string) */ $timezone="IST";
    public /** @type (string) */ $type;
    
    /**
     * @param string -filename
     * @returns calendar object
     */
     public function __construct() {
         parent::__construct();
     }
     
     /**
      * @param filename
      * @returns ical object
      */
     public function getICalFormat($filename) {
        //parent::construct();
        $this->_init('bookingsystem/calendar');
        if (!$filename) {
            return false;
        }
        $filetype = explode(".",$filename)[1];
        if($filetype=='ics') {
            $lines = file($filename, FILE_IGNORE_NEW_LINES | FILE_SKIP_EMPTY_LINES);
        
            if (stristr($lines[0], 'BEGIN:VCALENDAR') === false) 
                return false;
        
            $this->event_count=0;
        
            foreach($lines as $line) {
                $line = trim($line);
                $add = $this->keyValueFromString($line);
                list($keyword,$value) = $add;           
            
                switch ($line) {
                case "BEGIN:VEVENT":
                    $this->type = 'EVENT';
                    $this->event_count++;
                    $this->cal['EVENT'][$this->event_count]['source'] = $filename;
                    break;
                case "BEGIN:VCALENDAR":
                case "BEGIN:VTODO":
                case "BEGIN:VALARM":
                case "BEGIN:VJOURNAL":
                case "BEGIN:VTIMEZONE":
                case "BEGIN:VFREEBUSY":
                    $this->type = "";
                    break;
                }

                $this->addCalendarComponent($this->type,$keyword,$value);
            }
        }
        // if file is csv
        elseif($filetype=='csv') {
            $d = array_map('str_getcsv', file($filename));
    
            $columns = $d[0];
            $content = [];
            for ($i=1; $i < count($d); $i++) {
                array_push($content, array_combine($columns,$d[$i]));
            }
            $this->cal['EVENT']=array();
            $data=array();
            foreach ($content as $value) {
                $tmp = array();
                //format for time?
                $tmp['item_day_start_date'] = str_replace("/","-",$value['Start Date']) ." ". $this->fun($value['Start Time']);
                $tmp['item_day_end_date'] = str_replace("/","-",$value['End Date']) ." ". $this->fun($value['End Time']);
                $tmp['item_day_description'] = $value['Subject'];
                $tmp['source'] = $filename;
                array_push($this->cal['EVENT'],$tmp);
            }
        }
        return $this->cal;
    }

    public function fun($x) {
        $temp =  date('H:i:s',strtotime($x));
        return $temp;
    }

    /**
     *  @param ics line
     *  @return array
     */
    public function keyValueFromString($text) 
    {
        preg_match("/([^:]+)[:]([\w\W]*)/", $text, $matches);
        if (count($matches) == 0) {
            return false;
        }
        $matches = array_splice($matches, 1, 2);
        return $matches;
    }

    /**
     * @param key,value for time element in ics
     * @returns time in dd/mm/YYYY H:i:s format (verify) 
     */
    public function getTime($keyword,$value) {
        $time=$value;
        if(strpos($keyword,"TZID")!==false) {
            $tz=explode("=",$keyword)[1];
        }
        else {
            if($value[ strlen($value)-1 ]!='Z') 
                $tz=$this->timezone;
            else
                $tz="UTC";
            
            $time=$value;
        }
        
        $time=str_replace('T','',$time);
        $time=str_replace('Z','',$time);
        
        $df1 = "YmdHis"; //format of icalDate
        $df2 = "d/m/Y H:i:s"; //format to be converted to
        
        $d = DateTime::createFromFormat($df1,$time,new DateTimeZone($tz));
        
        $d->setTimezone(new DateTimeZone("IST"));
        $date = $d->format($df2);
       
        return $date;
        
    }

    /**
     * Adds event attributes to event element in cal object
     * @param typeof component,keyword,value
     */
    public function addCalendarComponent($type,$keyword,$value) {
        /**
         * Set timezone default timezone for the calendar if mentioned in X-WR-TIMEZONE
         */
        if($keyword=="X-WR-TIMEZONE") {
            $this->timezone= $value;
        }
        if ($this->type=='EVENT') {
            if (((strpos($keyword,'DTEND'))!==false) or (strpos($keyword,'DTSTART')!==false)) {
                //ignore events that take more than a day
                if(strpos($keyword,'DATE')!==false) {
                    $this->event_count--;
                    $this->type="";
                    return false;
                }
                else {
                    $time = $this->getTime($keyword,$value);
                    $keyword = explode(";",$keyword)[0];
                    if(strpos($keyword,'DTSTART')!==false)
                        $this->cal[$this->type][$this->event_count]['item_day_start_date'] = $time;
                    if(strpos($keyword,'DTEND')!==false)
                        $this->cal[$this->type][$this->event_count]['item_day_end_date'] = $time;
                }
            }
            if(strpos($keyword,'SUMMARY')!==false) {
                $this->cal[$this->type][$this->event_count]['item_day_description'] = $value;
            }
            if(strpos($keyword,'UID')!==false) {
                $this->cal[$this->type][$this->event_count]['event_id'] = $value;
            }
        }
    }    

     public function getVendorId() {
          $session = Mage::getSingleton('core/session');
          $vendorID = $session->getData('vendorId');
          return $vendorID;
     }
     
     public function getVendorIdFromName($name) {
         $vendorModel = Shoptimize_Vendor_Model_Vendor::_construct();
         $result = Shoptimize_Vendor_Model_Vendor::getVendorByName($name);
         return $result['id'];
     }

     /**
      * @param file
      * create ical object and insert into db
      */
     public function importICaltoDB($file,$vendorId) {
        $cal = $this->getICalFormat($file);
        $events = $this->events();

        
        foreach ($events as $event) {
           $event['source'] = $file;
           $event['vendor_id'] = $vendorId;
           $event['status']="available";
           if($this->validateEvent($vendorId,$event)) {
               
               $start_time=explode(" ",$event['item_day_start_date']);
               $end_time= explode(" ",$event['item_day_end_date']);
               $event['item_day_start_date']=$start_time[0];
               $event['item_day_end_date']=$end_time[0];

               Shoptimize_Bookingsystem_Model_Calendar::saveBookingCalendars($event);
           }
            //if conflicts in event schedule mail vendor
            else {
                // $session = Mage::getSingleton('core/resource');
                // $table = Mage::getSingleton('core/resource')->getTableName('vendor_users');
                // $readConnection = $session->getConnection('core_read');
                // $query = "SELECT email1,email2 FROM ".$table." WHERE id=".$vendorId;
                // $result = $readConnection->fetchAll($query);
                // $mails = array_pop($result);
                // $to = isset($mails['email1']) ? $mails['email1'] : $mails['email2'];

                // $subject = "Event Notification";
                $message = "Clash in event " . $event['item_day_description'] .' at time '.$event['item_day_start_date'].' : ' .$event['item_day_end_date'];
                Mage::log($message,null,'error.log');
                return $message;
                // if (!mail($to, $subject, $body)) 
                //     Mage::log('message not sent', null, 'error.log');
                
            }
        }
    }
     
     /**
      * @param string e.g 1,0,1
      * @returns time e.g 1:00:00 
      */
     public function converttoTime($t) {
         $array = explode(",",$t);
         if ($array[2]==2) {
             $array[0] = (string)((int)$array[0] + 12);
             echo "time 12 hour clock".$array[0]."\n";
         }
         $t = implode(":",$array);
         $time = substr($t,0,strlen($t) - 2);
         echo strtotime($time)."\n";
         Mage::log($time,null,'system.log',true);
         return  date('H:i:s',strtotime($time))."\n";
     }
     
     /**
      * @param event
      * @returns bool
      * time from array format Y-m-d H:i:s
      */
     public function validateEvent($vendorId,$event) {
         // get list of events by vendorId
         
         $start_time=explode(" ",$event['item_day_start_date']);
         $end_time=explode(" ",$event['item_day_end_date']);

         //fetch events of vendor from calendar falling on same day
         $collection = Shoptimize_Bookingsystem_Model_Calendar::getCalendarByVendorandDate($vendorId,$start_time[0],"");

         //return $collection->getFirstItem()->getId();
         if (is_null($collection->getFirstItem())) {
             return true; 
         }
        
         //get from booking_system events with the corresponding booking_id         
         foreach ($collection as $item) {
             $resource = Mage::getSingleton('core/resource');
             $readConnection = $resource->getConnection('core_read');
             $table =  Mage::getSingleton('core/resource')->getTableName('booking_system');
             $query = 'SELECT * from '.$table.' where booking_id='.$item->getBookingId();
             
             //get booked hours from 
             $results = $readConnection->fetchAll($query);

             foreach($results as $result) {
                 //check time for conflicts
                 $booking_st = $this->converttoTime($result['booking_service_start']);
                 $booking_et = $this->converttoTime($result['booking_service_end']);
                 if( ($booking_st <= $start_time[1]) and (booking_et>= $start_time))
                 {                     
                     return false;
                 }
                 else
                     return true;
             }
         }
     }

    /** 
     * @return array of events
     */
    public function events() {
        return $this->cal['EVENT'];
    }

    /**
     * @return bool
     */
    public function hasEvents() {
        if ($this->event_count==0)
            return false;
        else
            return true;
    }
     
     /**
      * @param d-m-Y H:i:s
      * @return YmdHis
      */
    public static function convertTime($time) {
        $df1 = "d-m-Y H:i:s";
        $df2 = "YmdHis";
        $tz="IST";
        $d = DateTime::createFromFormat($df1,$time);
        $date = $d->format($df2);
        return $date;
    }

     /**
      * @return vendor specific url for ics if present in DB else generates a uniqid and updates DB.
      */
     public function getUrl($vendorId) {
         $resource = Mage::getSingleton('core/resource');
         $readConnection = $resource->getConnection('core_read');
         $table =  Mage::getSingleton('core/resource')->getTableName('vendor_users');
         
         $query = "SELECT ical_url from vendor_users where id = {$vendorId}";
         $coll = $readConnection->fetchAll($query);

         $icalUrl = array_pop($coll);
         
         if (is_null($icalUrl['ical_url'])) {
             $icalUrl = uniqid();

             $writeConnection = $resource->getConnection('core_write');
             $query = 'UPDATE vendor_users set ical_url='. $icalUrl .' where id ='.$vendorId;
             $writeConnection->query($query);
             return $icalUrl;
         }
         return $icalUrl["ical_url"];         
     }
     
    /** @param vendor id
     * makes a call to calendar database and dumps content to file
     *  @return ics file name
     */
     public function converttoICS($vendorId) {
        $fname= 'misterevent.ics';

        $icalUrl= $this->getUrl($vendorId);
        
        $fileprefix = "media/".$icalUrl."/".$fname;
        file_put_contents($fname,"BEGIN:VCALENDAR\nVERSION:2.0\nCALSCALE:GREGORIAN\nX-WR-TIMEZONE:IST\n");

        $resource = Mage::getSingleton('core/resource');
        $readConnection = $resource->getConnection('core_read');
        $table =  Mage::getSingleton('core/resource')->getTableName('booking_calendar');         
        $query = "SELECT * from {$table} where vendor_id = {$vendorId}";
        $events = $readConnection->fetchAll($query);
        
        function getEvent($event) {
            $line="";
            foreach($event as $key=>$value) {
                switch ($key) {
                case "description":
                    $line = $line.'SUMMARY:'.$value."\n";
                    break;
                case "start_date":
                    $time = date('Ymd',strtotime($value));
                    $line = $line.'DTSTART;VALUE=DATE:'.$time."\n";
                    break;
                case "end_date":
                    $time = date('Ymd',strtotime($value));
                    $line = $line.'DTEND;VALUE=DATE:'.$time."\n";;
                    break;
                default:
                    echo "Default";
                }
            }
           echo $line; 
           return "BEGIN:VEVENT\n".$line."END:VEVENT\n";;
        }

            foreach($events as $event) {
                $line = getEvent($event);
                file_put_contents($fname,$line, FILE_APPEND | LOCK_EX);
            }
        file_put_contents($fname,"END:VCALENDAR",FILE_APPEND | LOCK_EX);
        return $fname;
    }

     /**
      * @param vendorid
      * makes a call to converttoICS and downloads renegarted ics file
      */
     public function downloadICS($vendorId) {
        $fname= 'misterevent.ics';
        $icalUrl= $this->getUrl($vendorId);
        $file = "media/".$icalUrl."/".$fname;

        
        if (!file_exists($file)) {
            $this->converttoICS($vendorId);
        }
        if (file_exists($file)) {
            $this->converttoICS($vendorId);
            header('Content-Description: File Transfer');
            header('Content-type: text/calendar; charset=utf-8');
            header('Content-Disposition: attachment; filename='.basename($file));
            header('Content-Length: ' . filesize($file));
            ob_clean();
            flush();
            readfile($file);
            exit;
        }
     }
}