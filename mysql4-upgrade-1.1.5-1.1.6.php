<?php
$installer = $this;
$installer->startSetup();
$tablePrefix = Mage::getConfig()->getTablePrefix();
$tableBookingCalendar = $tablePrefix.'booking_calendar';
$installer->getConnection()->addColumn($this->getTable($tableBookingCalendar), 'event_id', 'varchar(255)');
$installer->getConnection()->addColumn($this->getTable($tableBookingCalendar), 'source', 'varchar(255)');

$table = Mage::getSingleton('core/resource')->getTableName('vendor_users');
$installer->getConnection()->addColumn($this->getTable($tableBookingCalendar), 'vendor_id', 'varchar(255)');

/**
$installer->getConnection()->addIndex(
	$installer->getIdxName($tableBookingCalendar,'vendor_id'),
	'vendor_id');

$installer->getConnection()->addForeignKey(
	$installer->getFkName($tableBookingCalendar, 'vendor_id', $table, 'id'),
        'vendor_id', $installer->getTable($table), 'id',
        Varien_Db_Ddl_Table::ACTION_CASCADE, Varien_Db_Ddl_Table::ACTION_CASCADE);
*/
$installer->run(
	"ALTER TABLE ".$tableBookingCalendar." ADD FOREIGN KEY (vendor_id) REFERENCES ".$table."(id)"
);
$installer->endSetup(); 
?> 