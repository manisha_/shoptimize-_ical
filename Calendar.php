<?php
/**
 * @Author      : Magebay Team
 * @package     Booking System Pro
 * @copyright   Copyright (c) 2014 MAGEBAY (http://www.magebay.com)
 * @terms  http://www.magebay.com/terms
 * @license     http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
 **/
class Shoptimize_Bookingsystem_Model_Calendar extends Mage_Core_Model_Abstract
{
    public function _construct()
    {
        parent::_construct();
        $this->_init('bookingsystem/calendar');
    }
	function getCalendars($arrayseletct = array('*'),$conditions = array(),$orderBy = 'start_date',$sortOrder = 'ASC',$limit = 0,$curPage = 1)
	{
		$collection = Mage::getModel('bookingsystem/calendar')->getCollection();
		$collection->addFieldToSelect($arrayseletct);
		if(count($conditions))
		{
			foreach($conditions as $key => $condition)
			{
				$collection->addFieldToFilter($key,$condition);
			}
		}
		if($limit > 0)
		{
			$collection->setPageSize($limit);
		}
		$collection->setCurPage($curPage);
		$collection->setOrder($orderBy,$sortOrder);
		return $collection;
	}
	function getBookingCalendars($bookingId,$arrayseletct = array('*'),$conditions = array(),$orderBy = 'start_date',$sortOrder = 'ASC',$limit = 0,$curPage = 1)
	{
		$currDate = now(true);
		$collection = $this->getCalendars($arrayseletct,$conditions,$orderBy,$sortOrder,$limit,$curPage);
		$collection->addFieldToFilter('booking_id',$bookingId)
			->addFieldToFilter(
					array('end_date','default_value'),
					array(
						array('gteq'=>$currDate),
						array('eq'=>1)
						)
				);
		return $collection;
	}
	/* calendar for backend */
	function getBackendBookingCalendars($bookingId,$arrayseletct = array('*'),$conditions = array(),$orderBy = 'start_date',$sortOrder = 'ASC',$limit = 0,$curPage = 1)
	{
		$currDate = now(true);
		$collection = $this->getCalendars($arrayseletct,$conditions,$orderBy,$sortOrder,$limit,$curPage);
		$collection->addFieldToFilter('booking_id',$bookingId);
		return $collection;
	}
	function getCalendar($id)
	{
		$calendar = Mage::getModel('bookingsystem/calendar')->load($id);
		if($calendar->getId())
		{
			return $calendar;
		}
		return null;
	}
	function saveBookingCalendars($params)
	{
		$checkIn = '';
		$checkOut = '';
		$helper = Mage::helper('bookingsystem/booking');
		if(isset($params['item_day_start_date']))
		{
			$checkIn = $helper->convertFomaDate($params['item_day_start_date']);
		}
		if(isset($params['item_day_end_date']))
		{
			$checkOut = $helper->convertFomaDate($params['item_day_end_date']);
		}
		$defaultValue = isset($params['item_day_default_value']) ? 1 : 2;
		$params['item_day_price'] = (isset($params['item_day_price']) &&  $params['item_day_price'] > 0) ? $params['item_day_price'] : null;
		$params['item_day_qty'] = (isset($params['item_day_qty']) && $params['item_day_qty'] > 0) ? $params['item_day_qty'] : null;
		$params['item_day_promo'] = (isset($params['item_day_promo']) && $params['item_day_promo'] > 0) ? $params['item_day_promo'] : null;
        //        $params['vendor_id'] =  isset($params['vendor_id']) ? $params['vendor_id'] : null;
		$dataSave = array(
				'calendar_id'=>$params['calendar_id'],
				'start_date'=>$checkIn,
				'end_date'=>$checkOut,
				'qty'=>$params['item_day_qty'],
				'status'=>$params['item_day_status'],
				'price'=>$params['item_day_price'],
				'promo'=>$params['item_day_promo'],
				'group_day'=>2,
				'booking_id'=>$params['booking_id'],
				'description'=>$params['item_day_description'],
				'default_value'=>$defaultValue,
				'booking_type'=>$params['booking_type'],
                'vendor_id'=>$params['vendor_id'],
                'source'=>$params['source'],
		);
		if($dataSave['calendar_id'] == 0)
		{
			unset($dataSave['calendar_id']);
		}
		$this->setData($dataSave)->save();
	}
	//get calendars for search
	function getCurrentCalendars($checkIn,$checkOut,$arrayType)
	{
		//$checkIn = Mage::helper('bookingsystem/booking')->convertFomaDate($checkIn);
		//$checkOut = Mage::helper('bookingsystem/booking')->convertFomaDate($checkOut);
		$collection = $this->getCollection()
					->addFieldToSelect('*')
					->addFieldToFilter('booking_type',array('in'=>$arrayType));
		if($checkIn != '' && $checkOut == '')
		{
			$collection->addFieldToFilter(array('end_date','default_value'),
											array(
												array('gteq'=>$checkIn),
												array('eq'=>1)
											)
										);
		}
		elseif($checkIn == '' && $checkOut != '')
		{
			$collection->addFieldToFilter(array('start_date','default_value'),
											array(
												array('lteq'=>$checkOut),
												array('eq'=>1)
											)
										);
		}
		elseif($checkIn != '' && $checkOut != '')
		{
			$collection->addFieldToFilter(array('start_date','default_value'),
											array(
												array('lteq'=>$checkOut),
												array('eq'=>1)
											)
										);
			$collection->addFieldToFilter(array('end_date','default_value'),
											array(
												array('gteq'=>$checkIn),
												array('eq'=>1)
											)
										);
		}
		return $collection;
	}
	/* 
	* get item between days
	* @param int $bookingId, string $strDay
	* return $item
	*/
	function getCalendarBetweenDays($bookingId,$strDay,$bookingType = 'per_day')
	{
		//$strDay = Mage::helper('bookingsystem/booking')->convertFomaDate($strDay);
		$arrayseletct = array('*');
		$conditions = array();
		$orderBy = 'default_value';
		$sortOrder = 'DESC';
		$collection = $this->getCalendars($arrayseletct,$conditions,$orderBy,$sortOrder);
		$collection->addFieldToFilter('booking_id',$bookingId);
		$collection->addFieldToFilter('booking_type',$bookingType);
	/* 	$collection->addFieldToFilter(array('status','status'),
									array('available','special')
									); */
		$collection->addFieldToFilter(array('end_date','default_value'),
											array(
												array('gteq'=>$strDay),
												array('eq'=>1)
											)
										);
		$collection->addFieldToFilter(array('start_date','default_value'),
											array(
												array('lteq'=>$strDay),
												array('eq'=>1)
											)
										);
		return $collection->getFirstItem();
	}
	/**
	* delete calendar by bookingId
	* @param int $bookingId
	* @return $this;
	**/
	function deleteCalenderByBookingId($bookingId,$bookingType = 'per_day')
	{
		$arrayseletct = array('*');
		$conditions = array('booking_type'=>$bookingType);
		$collection = $this->getBookingCalendars($bookingId,$arrayseletct,$conditions);
		if(count($collection))
		{
			foreach($collection as $collect)
			{
				$this->setId($collect->getId())->delete();
			}
		}
        return $collecion;
	}
    public function getCalendarByVendorandDate($vendorId,$start_date,$bookingType = 'per_day') {
		$arrayseletct = array('*');
		$conditions = array('booking_type'=>$bookingType);        
		$collection = $this->getCalendarBetweenDays($arrayseletct,$start_date,$bookingType);
        $collection= $this->getCollection()
                   ->addFieldToFilter('vendor_id',$vendorId)
                   ->addFieldToFilter('source',$source);
        return $collection;
    }
    public function getCalendarBySource($vendorId,$source,$bookingType = 'per_day')
    {
        $arrayseletct = array('*');
		$conditions = array();
		$orderBy = 'default_value';
		$sortOrder = 'DESC';
		$collection = $this->getCalendars($arrayseletct,$conditions,$orderBy,$sortOrder);
        $collection->addFieldToFilter('vendorId',$vendorId);
        $collection->addFieldToxFilter('source',$source);
        return $collection;
    }        
}
?>
